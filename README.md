===========================
Project Cartographer Server
===========================

====== Requirements =======
- Python 2.7.6
- Any others?

===== Documentation =====

There's two different server daemons currently,

Server.py and h2master.py

======= Server.py =======

The broadcast server,
It's pretty simple it just adds clients into a dictionary with their source port and addresses and relays packets to the whole group acting as a multi-cast.

======= h2master.py ======

The actual master server it handles quite a bit and should handle even more in the future... there needs to be some decision made on how the server structure
will actually work.

Currently there's several things that need to be taken care of (NAT punchthrough, and handling of sending clients needed information about other clients).

In the future clients will have static information provided by the H2Vista API (which should be cached until a client disconnects).

This means we require a ping/pong to the master server AND to the "Host" they're connected to.

This heart beat to the master will tell us when the information is no longer needed and we can clean-up.